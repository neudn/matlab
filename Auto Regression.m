%% Part B, Q2
a1(1) = 1; alpha_1 = 0.1;
a2(1) = 1; alpha_2 = 0.7;
a3(1) = 1; alpha_3 = -0.1;
a4(1) = 1; alpha_4 = -0.7;
a5(1) = 1; alpha_5 = 1;
for i = 2:10
    a1(i) = a1(i-1) * alpha_1;
    a2(i) = a2(i-1) * alpha_2;
    a3(i) = a3(i-1) * alpha_3;
    a4(i) = a4(i-1) * alpha_4;
    a5(i) = a5(i-1) * alpha_5;
end
figure
subplot(3,2,1)
bar(a1)
title('alpha = 0.1')
subplot(3,2,2)
bar(a2)
title('alpha = 0.7')
subplot(3,2,3)
bar(a3)
title('alpha = -0.1')
subplot(3,2,4)
bar(a4)
title('alpha = -0.7')
subplot(3,2,[5 6])
bar(a5)
title('alpha = 1')