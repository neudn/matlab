for i = 1:100000
    X(i) = normrnd(20,10);
end
figure;
subplot(1,2,1);
hist(X,40);
axis([-40 80 0 10000]);
xlabel('histogram');
mu = 20;
sigma = 10;
ix = -6*sigma:0.001:6*sigma;
iy = pdf('normal', ix, mu, sigma);
subplot(1,2,2);
plot(ix,iy);
axis([-40 80 0 0.05]);
xlabel('PDF');

figure;
subplot(1,4,1);
plot(ix,iy);
axis([-40 80 0 0.05]);
xlabel('PDF');
subplot(1,4,2);
hist(X,40);
axis([-40 80 0 10000]);
xlabel('Bin = 40');
subplot(1,4,3);
hist(X,60);
axis([-40 80 0 6700]);
xlabel('Bin = 60');
subplot(1,4,4);
hist(X,100);
axis([-40 80 0 4000]);
xlabel('Bin = 100');