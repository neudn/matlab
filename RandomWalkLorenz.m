x(1) = 1.5;
y(1) = 0.5;
z(1) = 1;

dt = 0.01;
sigma = 8;
beta = 8/3;
rho = 20;

for i = 2:10000;
    x(i) = x(i-1) + dt*sigma*(y(i-1)-x(i-1));
    y(i) = y(i-1) + dt*(x(i-1)*(rho-z(i-1))-y(i-1));
    z(i) = z(i-1) + dt*(x(i-1)*y(i-1)-beta*z(i-1));
end 
figure;
% subplot(1,3,1);
% plot(x,y,'*');
% subplot(1,3,2);
% plot(y,z,'*');
% subplot(1,3,3);
% plot(z,x,'x');
plot3(x,y,z);