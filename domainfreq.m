clear
num_values = 2000;
for i = 1:num_values
    t(i) = (i-1)/num_values;
end
freq = 100;
normalized_freq = (2*freq)/num_values
for i = 1:num_values
    x(i) = cos(2*pi*t(i)*freq);
end
figure(1)
plot(t,x)
axis([0 1 -1.1 1.1])
xlabel('Time -->')
ylabel('X -->')
figure(2)
periodogram(x)
figure(3)
pwelch(x)


for i = 1:num_values
    xn(i) = normrnd(0,0.02);
end
figure(4)
plot(t,xn)
xlabel('Time -->')
ylabel('X -->')
figure(5)
periodogram(xn)
figure(6)
pwelch(xn)

alpha1 = 0.7;
xar1p(1) = normrnd(0,0.02);
for i = 2:num_values
    xar1p(i) = alpha1*xar1p(i-1)+ normrnd(0,0.02);
end
figure(7)
plot(t,xar1p)
xlabel('Time -->')
ylabel('X -->')
figure(8)
periodogram(xar1p)
figure(9)
pwelch(xar1p)

alpha2 = -0.7;
xar1n(1) = normrnd(0,0.02);
for i = 2:num_values
    xar1n(i) = alpha2*xar1n(i-1)+ normrnd(0,0.02);
end
figure(10)
plot(t,xar1n)
xlabel('Time -->')
ylabel('X -->')
figure(11)
periodogram(xar1n)
figure(12)
pwelch(xar1n)

xrw(1) = normrnd(0,0.02);
for i = 2:num_values
    xrw(i) = xrw(i-1)+ normrnd(0,0.02);
end
figure(13)
plot(t,xrw)
xlabel('Time -->')
ylabel('X -->')
figure(14)
periodogram(xrw)
figure(15)
pwelch(xrw)

