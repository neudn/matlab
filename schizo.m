load schizo.dat;
time=1:length(schizo);
Tranquilizer_Effect=0;

validity=0;
while validity~=1
    Tranquilizer_Effect = input('Do you want separate analysis after tranquilizer? (Y/N):  ','s');
    if (Tranquilizer_Effect=='Y') | (Tranquilizer_Effect=='N')
        validity=1;
    end 
end

if Tranquilizer_Effect=='N'
    mean_schizo(1:length(schizo))=mean(schizo);
else
    mean_schizo(1:60)=mean(schizo(1:60));
    mean_schizo(61:length(schizo))=mean(schizo(61:length(schizo)));
end 

mavg_wsize=5;
validity=0;
while validity~=1
    mavg_wsize = input ...
  ('Provide the moving average window size (3-7, odd only, default 5):  ');
    if (mavg_wsize>=2) | (mavg_wsize<=10)
        if (mavg_wsize/2 - round(mavg_wsize/2)) ~= 0
            validity=1;
        end
    end
end

n_dangle=(mavg_wsize-1)/2;
for i=1:n_dangle
    Moving_Average_schizo(i)=schizo(i);
end
for i=(length(schizo-n_dangle+1):length(schizo))
    Moving_Average_schizo(i)=schizo(i);
end
for i=(n_dangle+1):(length(schizo)-n_dangle)
    Moving_Average_schizo(i)=0;
    for j=(i-n_dangle):(i+n_dangle)
        Moving_Average_schizo(i)=Moving_Average_schizo(i)+schizo(j);
    end
    Moving_Average_schizo(i)=Moving_Average_schizo(i)/mavg_wsize;
end

schizo_no_tranquilizer=schizo(1:60);
schizo_tranquilizer=schizo(61:120);
[H,P,CI,STATS]=ttest2(schizo_no_tranquilizer,schizo_tranquilizer,0.05,1);
H

trend_no_tranquilizer=schizo_no_tranquilizer-detrend(schizo_tranquilizer);
if Tranquilizer_Effect=='Y'
    trend_schizo(1:60)=schizo(1:60)-detrend(schizo(1:60));
    trend_schizo(61:120)=schizo(61:120)-detrend(schizo(61:120));
else
    trend_schizo=schizo-detrend(schizo);
end

phase_1st_no_tranq=schizo(1:59);
phase_2nd_no_tranq=schizo(2:60);
phase_1st_tranq=schizo(61:119);
phase_2nd_tranq=schizo(62:120);
phase_1st=schizo(1:119);
phase_2nd=schizo(2:120);

figure(1)
subplot(121)
plot(schizo)
title('Schizo Data')
xlabel('Time in Days -->')
ylabel('Perceptual Days -->')
subplot(122)
plot(time,schizo,'b*',time,mean_schizo,'r-')

figure(2)
subplot(221)
plot(schizo)
title('Raw Data')
subplot(222)
plot(Moving_Average_schizo)
title('Moving Average')
subplot(223)
plot(trend_schizo)
title('Trends')
subplot(224)
plot(phase_1st,phase_2nd,'b*')
xlabel('X(t-1)')
ylabel('X(t)')

figure(3)
subplot(311)
plot(phase_1st,phase_2nd,'b*')
title('All Data')
xlabel('X(t-1)')
ylabel('X(t)')
subplot(312)
plot(phase_1st_no_tranq,phase_2nd_no_tranq,'b*')
title('1st half Without Tranquilizer')
xlabel('X(t-1)')
ylabel('X(t)')
subplot(313)
plot(phase_1st_tranq,phase_2nd_tranq,'b*')
title('2nd half With Tranquilizer')
xlabel('X(t-1)')
ylabel('X(t)')







    
    
    
    
    
    
    
    





