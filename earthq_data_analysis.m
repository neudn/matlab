load earthq.dat;
start_year=1900;
for i = 1:length(earthq)
    year(i) = start_year+(i-1);
    numeq(i) = earthq(i);
end
end_year=year(length(earthq));

figure(1)
plot(year,numeq,'b-')
grid
axis_buffer = 5;
axis([start_year end_year min(earthq)-axis_buffer max(earthq)+axis_buffer])
title('Number of Earthquakes of Magnitude > 7')
xlabel('Years')
ylabel('Number of large earthquakes')

meaneq = mean(earthq);
stdeq = sqrt(var(earthq));
for i = 1:length(earthq)
    meanline(i) = meaneq;
    ucline(i) = meaneq + 3*stdeq;
    lcline(i) = meaneq - 2*stdeq;
end
trendeq = earthq - detrend(earthq);

figure(2)
plot(year,numeq,'b-',year,meanline,'ko',year,ucline,'r*',year, ...
    lcline,'g*', year, trendeq,'k--')
axis_buffer = 10;
axis([start_year end_year min(lcline)-axis_buffer max(ucline)+axis_buffer])
title('Number of Earthquakes of Magnitude > 7')
xlabel('Years')
ylabel('Number of large earthquakes')
legend('Count', 'Mean', 'Upper 3-Sigma Bound', ...
    'Lower 2-Sigma Bound', 'Trend Line',-1)

figure(3)
hist(earthq)
title('Histogram of Earthquake Data')
xlabel('Number of Earthquakes per Year')
ylabel('Frequency')

for i=2:length(earthq)
    current(i)=earthq(i);
    lagged(i)=earthq(i-1);
end
figure(4)
scatter(lagged,current)
title('Scatter plot of current vs. lag-1 values')

figure(5)
maxlags=10;
autocorr=xcorr(earthq,maxlags,'coeff');
plotlag=0:maxlags;
bar(plotlag,autocorr((maxlags+1):(2*maxlags+1)))
title('Autocorrelation at multiple lags')
xlabel('Lag')
ylabel('Autocorrelation')

figure(6)
Hs=spectrum.periodogram;
psd(Hs,earthq);

model = armax(earthq,[3 1]);
figure(7)
compare(earthq, model, 1);
figure(8)
compare(earthq, model, 2);
figure(9)
compare(earthq, model, 3);
figure(10)
compare(earthq, model, 4);

figure(11)
plot(lagged,current,'r*')
title('Lag-1 Phase Space')
xlabel('Earthquake Year = (Y-1)')
ylabel('Earthquake Year = Y')
grid

