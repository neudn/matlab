% This is the same code as armademo.m in the tutorial with a few changes

% A Hodgepodge code for time series demonstration and tutorial
% Simulation / Analysis of simple ARMA (stationary) processes

clear



%Simulations

% AR(1) model with +ve alpha
alpha1=0.5;
x1(1)=normrnd(0,0.25);
for i=2:1000
    x1(i)=alpha1*x1(i-1)+normrnd(0,0.25);
end
% AR(1) model with -ve alpha
alpha2=-0.5;
x2(1)=normrnd(0,0.25);
for i=2:1000
    x2(i)=alpha2*x2(i-1)+normrnd(0,0.25);
end
% MA(1) model with +ve beta
beta1=0.5;
zt=normrnd(0,0.25,1,1000);
x3(1)=zt(1);
for i=2:1000
    x3(i)=beta1*zt(i-1)+zt(i);
end
% MA(1) model with -ve beta
beta2=-0.5;
zt=normrnd(0,0.25,1,1000);
x4(1)=zt(1);
for i=2:1000
    x4(i)=beta2*zt(i-1)+zt(i);
end
% ARMA(1,1) model with +ve alpha and +ve beta
alpha3=0.5;
beta3=0.5;
zt=normrnd(0,0.25,1,1000);
x5(1)=zt(1);
for i=2:1000
    x5(i)=alpha3*x5(i-1)+beta3*zt(i-1)+zt(i);
end
% ARMA(1,1) model with -ve alpha and +ve beta
alpha4=-0.5;
beta4=0.5;
zt=normrnd(0,0.25,1,1000);
x6(1)=zt(1);
for i=2:1000
    x6(i)=alpha4*x6(i-1)+beta4*zt(i-1)+zt(i);
end
figure
subplot(321)
plot(x1)
title('AR(1): a>0')
subplot(322)
plot(x2)
title('AR(1): a<0')
subplot(323)
plot(x3)
title('MA(1): b>0')
subplot(324)
plot(x4)
title('MA(1): b<0')
subplot(325)
plot(x5)
title('ARMA(1,1): a>0,b>0')
subplot(326)
plot(x6)
title('ARMA(1,1): a<0,b>0')



% Time Domain: Autocorrelations
maxlags=10;
plotlag=0:maxlags;
autocorr1=xcorr(x1,maxlags,'coeff');
autocorr2=xcorr(x2,maxlags,'coeff');
autocorr3=xcorr(x3,maxlags,'coeff');
autocorr4=xcorr(x4,maxlags,'coeff');
autocorr5=xcorr(x5,maxlags,'coeff');
autocorr6=xcorr(x6,maxlags,'coeff');
figure
subplot(321)
bar(plotlag,autocorr1((maxlags+1):(2*maxlags+1)))
title('ACF for AR(1) a>0')
subplot(322)
bar(plotlag,autocorr2((maxlags+1):(2*maxlags+1)))
title('ACF for AR(1) a<0')
subplot(323)
bar(plotlag,autocorr3((maxlags+1):(2*maxlags+1)))
title('ACF for MA(1) b>0')
subplot(324)
bar(plotlag,autocorr4((maxlags+1):(2*maxlags+1)))
title('ACF for MA(1) b<0')
subplot(325)
bar(plotlag,autocorr5((maxlags+1):(2*maxlags+1)))
title('ACF for ARMA(1,1) a>0,b>0')
subplot(326)
bar(plotlag,autocorr6((maxlags+1):(2*maxlags+1)))
title('ACF for ARMA(1,1) a<0,b>0')



% Frequency Domain: Power Spectrum
sampling_freq=1/1000;
h=spectrum.welch;
Hpsd1=psd(h,x1,'Fs',sampling_freq);
Hpsd2=psd(h,x2,'Fs',sampling_freq);
Hpsd3=psd(h,x3,'Fs',sampling_freq);
Hpsd4=psd(h,x4,'Fs',sampling_freq);
Hpsd5=psd(h,x5,'Fs',sampling_freq);
Hpsd6=psd(h,x6,'Fs',sampling_freq);
figure
subplot(321)
plot(Hpsd1)
title('AR(1), a>0')
subplot(322)
plot(Hpsd2)
title('AR(1), a<0')
subplot(323)
plot(Hpsd3)
title('MA(1), b>0')
subplot(324)
plot(Hpsd4)
title('MA(1), b<0')
subplot(325)
plot(Hpsd5)
title('ARMA(1,1), a>0,b>0')
subplot(326)
plot(Hpsd6)
title('ARMA(1,1), a<0,b>0')



% Comparison: White Noise
x7=normrnd(0,0.25,1,1000);
x8=normrnd(0,0.25,1,1000);
figure
subplot(321)
plot(x7)
title('WN: Sim. 1')
subplot(322)
plot(x8)
title('WN: Sim. 2')
autocorr7=xcorr(x7,maxlags,'coeff');
autocorr8=xcorr(x8,maxlags,'coeff');
subplot(323)
bar(plotlag,autocorr7((maxlags+1):(2*maxlags+1)))
title('ACF for WN: Sim. 1')
subplot(324)
bar(plotlag,autocorr8((maxlags+1):(2*maxlags+1)))
title('ACF for WN - Sim. 2')
Hpsd7=psd(h,x7,'Fs',sampling_freq);
Hpsd8=psd(h,x8,'Fs',sampling_freq);
subplot(325)
plot(Hpsd7)
title('PSD for WN: Sim. 1')
subplot(326)
plot(Hpsd8)
title('PSD for WN: Sim. 2')


% Comparison: Seasonal Signal ACF
amplitude=10;
freq=50;
sampling_freq=1/1000;
for i=1:1000
    s(i)=amplitude*cos(2*pi*freq*i*sampling_freq);
end
autocorr9=xcorr(s,maxlags,'coeff');
figure
subplot(211)
plot(s)
axis([0 1000 -15 15])
title('Seasonal Signal')
subplot(223)
bar(plotlag,autocorr9((maxlags+1):(2*maxlags+1)))
title('ACF for Seasonal Signal')
subplot(224)
Hpsd9=psd(h,s,'Fs',sampling_freq);
plot(Hpsd9)


% Ensemble of AR(1) signals
alpha1=0.3;
normmu=0;
normsig=0.4;
numensembles=100;
for j=1:numensembles
    xx(1)=normrnd(normmu,normsig);
    for i=2:1000
        xx(i)=alpha1*xx(i-1)+normrnd(normmu,normsig);
    end
    ymu(j)=mean(xx);
    yvar(j)=var(xx);
end
ymutheory=0;
figure
hist(ymu)
title('Ensemble Mean of AR(1)')
yvartheory=normsig*normsig/(1-alpha1*alpha1);
figure
hist(yvar)
title('Ensemble Variance of AR(1)')


% Periodogram and Smoothed Periodogram
figure
subplot(321)
periodogram(x1)
subplot(322)
periodogram(x2)
subplot(323)
periodogram(x3)
subplot(324)
periodogram(x4)
subplot(325)
periodogram(x5)
subplot(326)
periodogram(x6)

figure
subplot(321)
pwelch(x1)
subplot(322)
pwelch(x2)
subplot(323)
pwelch(x3)
subplot(324)
pwelch(x4)
subplot(325)
pwelch(x5)
subplot(326)
pwelch(x6)



