for i = 1:1000;
    x(i) = normrnd(0,1);
    y(i) = normrnd(0,1);
    z(i) = normrnd(0,1);
end 
figure;
subplot(1,3,1);
scatter(x,y);
subplot(1,3,2);
scatter(y,z);
subplot(1,3,3);
scatter(z,x);